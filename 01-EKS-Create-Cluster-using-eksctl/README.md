# EKS - Create Cluster

## List of Topics 
- Install CLIs
  - AWS CLI
  - kubectl
  - eksctl
- Create EKS Cluster
- Create EKS Node Groups
- Understand EKS Cluster Pricing
  - EKS Control Plane
  - EKS Worker Nodes
  - EKS Fargate Profile
- Delete EKS Clusters 


![EKS](./media/eks_cluster.png)

![EKS](./media/eks_control_node.png)

![EKS](./media/ekc_fargate_vpc.png)